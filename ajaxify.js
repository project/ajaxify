function ajaxify(elem, l, callback) {
  $(elem).empty().append('<img id ="ajaxifyimg" src="'+ ajaxify_loader +'" />');
  // Get HTML content
  $.ajax({
    type: "GET",
    dataType: "html",
    data: "output=html",
    cache: false,
    url: l,
    error: function(html){
      $(elem).empty().append(html);
    },
    success: function(html){
      $(elem).empty().append(html);
      if (callback) {
        $(document).ready(callback);
      }
    }
  });
  // Get JavaScript
  $.ajax({
    type: "GET",
    dataType: "json",
    data: "output=js",
    url: l,
    success: function(html) {
      // Load all JavaScript
      jQuery.each(html, function(i, val) {
        $.getScript(val);
      });
    }
  });
  return false;
}

/*
 * Ajaxify all links in retrieved content
 */
function ajaxify_content(elem) {
  // Ajaxify all links and forms
  $(elem).empty().append(html)
  .find('a').click(function(){
    return ajaxify(elem, 'ajaxify/'+ elem.replace('#', '=') + '/' + $(this).attr('href').substr($(this).attr('href').indexOf(base_url) + base_url.length));
  });
}
