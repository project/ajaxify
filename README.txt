AJAXify
Author: Omar Abdel-Wahab
Developed by: OpenCraft

About:
======
Loads a specific HTML element from other pages via AJAX.
